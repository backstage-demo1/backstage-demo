# Добро пожаловать!
Вы подключились к API: auth-keycloak-oidc/from-lowcode-synapse-admin-api
### Компонент user-app
В каталоге компонентов вы можете найти свой компонент user-app.
### Проверка подключения
Для проверки подключения выполнить команду:
curl -I -H "Authorization: Bearer <token>" <ingress-host>