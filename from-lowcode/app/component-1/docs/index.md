# Добро пожаловать!
Вы подключились к API: auth-keycloak-oidc/from-lowcode-synapse-admin-api
### Компонент component-1
В каталоге компонентов вы можете найти свой компонент component-1.
### Проверка подключения
Для проверки подлкючения выполниль команду:
```sh
curl <Адрес> -I -H "Authorization: Bearer <Токен>"
```
