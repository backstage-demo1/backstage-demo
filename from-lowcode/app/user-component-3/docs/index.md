# Добро пожаловать!
Вы подключились к API: auth-keycloak-oidc/from-lowcode-synapse-admin-api
### Компонент user-component-3
В каталоге компонентов вы можете найти свой компонент user-component-3.
### Проверка подключения
Для проверки подлкючения выполниль команду:
```sh
curl <Адрес> -I -H "Authorization: Bearer <Токен>"
```
