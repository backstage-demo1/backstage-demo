#RateLimit
for i in {1..20}; do curl http://178.170.193.187:2222/synapse -I; done

#Auth
#Invalid token
curl http://178.170.193.187:2222/synapse -I -H "Authorization: Bearer invalidToken"

#Valid token
token=$(curl https://raw.githubusercontent.com/istio/istio/release-1.17/security/tools/jwt/samples/demo.jwt -s); curl http://178.170.193.187:2222/synapse -I -H "Authorization: Bearer $token"
