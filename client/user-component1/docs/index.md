# Добро пожаловать!
Вы подключились к API: api:auth-keycloak-oidc/api
### Компонент user-component1
В каталоге компонентов вы можете найти свой компонент user-component1.
### Проверка подключения
Для проверки подключения выполнить команду:
```sh
curl -I -H "x-token: <token>" <ingress-host>
```
