# Добро пожаловать!
Вы подключились к API: api:auth-keycloak-oidc/api
### Компонент user-component8
В каталоге компонентов вы можете найти свой компонент user-component8.
### Проверка подключения
Для проверки подключения выполнить команду:
```sh
curl -I -H "x-token: <token>" <ingress-host>
```
