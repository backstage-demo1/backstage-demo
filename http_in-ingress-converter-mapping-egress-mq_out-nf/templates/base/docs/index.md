# Сервис получения деталей продукта

Настройки подключения к API

| Параметр  | Значение|
| ------------- |:-------------:|
| path      | /service/GetLegalOpportunity     |
| port      | 5443     |

Request Body Example

```
{
    "GetLegalOpportunityRq": {
        "RqUID": "cm7qvqthpZE0jWpFCl6C9twPwAE7B0j0",
        "rqTm": "2730-41-83T61:59:18",
        "spName": "string",
        "method": "string",
        "positionId": "string",
        "login": "string"
    }
}

```