apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: mqgateway-sp
    name: mqgateway-sp
  name: mqgateway-sp
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      name: mqgateway-sp
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      annotations:
        sidecar.istio.io/inject: 'true'
        sidecar.istio.io/proxyCPU: 200m
        sidecar.istio.io/proxyCPULimit: 200m
        sidecar.istio.io/proxyMemory: 200Mi
        sidecar.istio.io/proxyMemoryLimit: 200Mi
      labels:
        app: mqgateway-sp
        name: mqgateway-sp
    spec:
      containers:
        - name: mqgateway-sp
          image: swr.ru-moscow-1.hc.sbercloud.ru/sber/sbt_dev/ci90000087_asgtmq_dev/mq-gateway:2.8.0-46
          imagePullPolicy: Always
          ports:
            - containerPort: 5454
              protocol: TCP
            - containerPort: 8787
              protocol: TCP
          resources:
            limits:
              cpu: 200m
              memory: 300Mi
            requests:
              cpu: 100m
              memory: 200Mi
          env:
            - name: PROJECT_NAME
              valueFrom:
                fieldRef:
                  apiVersion: v1
                  fieldPath: metadata.namespace
          envFrom:
            - configMapRef:
                name: mqgateway-sp-config
          securityContext:
            readOnlyRootFilesystem: true
            privileged: false
            runAsNonRoot: false
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /actuator/health
              port: 8787
              scheme: HTTP
            initialDelaySeconds: 120
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /actuator/health
              port: 8787
              scheme: HTTP
            initialDelaySeconds: 120
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
            - mountPath: /deployments/config
              name: application-config
              readOnly: true
            - mountPath: /opt/synapse/logs
              name: synapselogs
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: { }
      terminationGracePeriodSeconds: 30
      volumes:
        - configMap:
            defaultMode: 0400
            items:
              - key: application.yml
                path: application.yml
            name: mqgateway-sp-config
          name: application-config
        - emptyDir: { }
          name: synapselogs

---
kind: ConfigMap
apiVersion: v1
metadata:
  name: mqgateway-sp-config
data:
  application.yml: |-
    server:
      systemName: system
      port: 8787
      # признак необходимости преобразования JSON-XML
      convertRequired: false
      # максимальное число потоков, принимающих запросы по http
      maxPoolSize: 1
    mq:
      system-name: system
      systemType: sp
      workMode: async
      connection:
        connections: [ {
          'channel': CLOUD.SYT,
          'queueManager': SYNAPSE.DEV.MGR,
          'hostname': 188.72.109.195,
          'port': 1452,
        } ]
        sendToCustomDestination: true
        sendQueue: SYNAPSE.AS2.REQUEST
        receiveQueue: SYNAPSE.AS2.RESPONSE
      monitoring:
        messages:
          expiry-listener:
            enable: true
            expiryTime: 40000
            checkInterval: 60000
            threadPoolSize: 1
      sync-receiver:
        #число потоков выполняющих чтение - разбор очереди
        poolSize: 20
        #отложить время чтения ответа на ms
        startDelay: 1
        #максимальный делей переповтора.
        maxDelay: 10
        #timeout на чтение
        maxProcessingTime: 100000
        #время сколько будем ждать получения ответов при остановке шлюза
        maxStopTime: 100000
        receiveTimeout: 50
        #Где ожидать ответного сообщения (workMode=sync) front if
        defaultReceiveQueue: SYNAPSE.AS2.SYNC.RESPONSE
    grpc:
      server:
        serverPort: 5454
      client:
        settings:
          default:
            hostname: unimapper-svc
            port: 5454
    transform:
      http-mq:
        # признак необходимости конвертации из HTTP в MQ
        httpMqRequired: true
        mqmd:
          ReplyToQ:
            - type: fromConst
              value: SYNAPSE.AS2.REQUEST
          MsgId:
            - type: fromConst
              value: '112233'
          CorrelId:
            - type: fromConst
              value: '112233'
          MsgType:
            - type: fromConst
              value: '1'
          Persistence:
            - type: fromConst
              value: '1'
    tracing:
      generateXB3Headers: 'true'
      generate128bitTraceId: 'true'
      tracingHeaders:
        x-synapse-rquid:
          - { type: fromBody, value: //RqUID }
          - { type: fromConst, value: 123123}
    routing:
      routeName:
        valueFrom:
          - type: fromConst
            value: default
      routeList:
        - routeName: default
          systemHeaders:
            ReplyToQ:
              - type: fromConst
                #Схема для маппера если через grpc front if (ReplyToQ_HEADER_VALUE_CONSUMER)
                value: response
          grpcHeaders:
            x-synapse-operationname:
              - type: fromConst
                value: add
          copyHeadersFromRequestSettings:
            copyAllMqmdHeaders: true
            copyAllUsrHeaders: true
          destinationExpression: "'unimapper-svc'"

---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: mqgateway-sp-svc
  name: mqgateway-sp-svc
spec:
  ports:
    - name: grpc
      protocol: TCP
      port: 5454
      targetPort: 5454
    - name: http
      protocol: TCP
      port: 8787
      targetPort: 8787
  selector:
    app: mqgateway-sp
  type: ClusterIP