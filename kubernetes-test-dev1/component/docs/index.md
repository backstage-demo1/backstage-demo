# Добро пожаловать!
Вы подключились к API: api:auth-keycloak-oidc/api
### Компонент user-component
В каталоге компонентов вы можете найти свой компонент user-component.
### Проверка подключения
Для проверки подключения выполнить команду:
```sh
curl -I -H "Authorization: Bearer <token>" <ingress-host>
```
